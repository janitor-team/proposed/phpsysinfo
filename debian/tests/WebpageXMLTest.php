<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;

class WebpageXMLTest extends TestCase
{

    public function testOutputJson(): void
    {
        $webpage = new WebpageXML("complete");
        $json = $webpage->getJsonString();
        $this->assertStringContainsString('"version":"3.', $json);
        $this->assertStringContainsString('CPULoad', $json);
        $json = json_decode($json);
        $this->assertStringContainsString('3.', $json->Generation->{'@attributes'}->version);
        $this->assertEquals((object) [], $json->Errors, 'should not have errors');
        $this->assertEquals((object) [], $json->Plugins, 'should be empty, no plugins are configured');
    }

    public function testOutputXml(): void
    {
        $webpage = new WebpageXML("complete");
        $out = $webpage->getXMLString();
        $xml = new DOMDocument();
        $xml->loadXML($out);
        $this->assertEmpty(libxml_get_errors());
        $xmlString = $xml->saveXML();
        $this->assertStringContainsString('<Generation version="3.', $xmlString);
        $this->assertStringContainsString('LoadAvg="', $xmlString);
        $this->assertStringContainsString('<Errors/>', $xmlString, 'should not have errors (expected empty node)');
        $this->assertStringContainsString('<Plugins/>', $xmlString, 'should be empty, no plugins are configured (expected empty node)');
        $this->assertStringContainsString('</tns:phpsysinfo>', $xmlString);
        // TODO: fix schema or contents
        //$xml->schemaValidate(PSI_APP_ROOT . '/phpsysinfo3.xsd');
        //$this->assertEmpty(libxml_get_errors());
    }
}
