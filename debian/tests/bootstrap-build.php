<?php

/**
 * define the application root path on the webserver
 * @var string
 */
define('PSI_APP_ROOT', realpath(__DIR__ . '/../../'));

require_once __DIR__ . '/bootstrap-autopkgtests.php';
